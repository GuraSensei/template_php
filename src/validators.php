<?php 

function userValidator($login, $passwd, $passwd2) {
    if ($passwd == $passwd2 && Model::factory('User')->where('login',$login)->find_many == []) {
        return preg_match('#^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,12}$#', $passwd) && preg_match('#^.{3,13}#', $login); 
        // password must contain at least 1 digit, 1 lower case, 1 upper case and have between 8 and 12 characters
        // login must have 3 characters and no more than 13
    }
}
