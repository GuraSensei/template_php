<?php require "vendor/autoload.php";

use \Tamtamchik\SimpleFlash\Flash;
use \Respect\Validation\Exceptions\NestedValidationException;

session_start();

$loader = new Twig\Loader\FilesystemLoader(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig\Extension\DebugExtension()); // Add the debug extension
    $twig->addFunction(new Twig\TwigFunction('messages', function () { //add flash message
        return Flash::display();
    }));

});

Flight::map('render', function ($template, $data = array()) {
    Flight::view()->display($template, $data);
});

/*Flight::before('start', function (&$params, &$output) {
    ORM::configure('sqlite:portfolio.db');
});*/ // Si une db doit être utilisée

Flight::route('/', function() {
    $data = index();
    Flight::render('index.twig', $data);
});

 Flight::route('/about', function(){
    $data = about();
    Flight::render('about.twig', $data);
 });

Flight::start();